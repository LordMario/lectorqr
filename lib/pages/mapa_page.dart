import 'dart:async';

import 'package:flutter/material.dart';
import 'package:qr_flutter/models/scan_model.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapaPage extends StatefulWidget {
  @override
  State<MapaPage> createState() => _MapaPageState();
}

class _MapaPageState extends State<MapaPage> {
  Completer<GoogleMapController> controlador = Completer();
  MapType tipoMapa = MapType.normal;
  @override
  Widget build(BuildContext context) {
    final ScanModel scan = ModalRoute.of(context)!.settings.arguments as ScanModel;
    final coordenadas = scan.valor.substring(4).split(',');
    final lat = double.parse( coordenadas[0]);
    final lng = double.parse( coordenadas[1]);
    final CameraPosition puntoInicial = CameraPosition(
      target: LatLng(lat,lng),
      zoom: 14.4746,
      tilt: 90.0,
    );
    Set <Marker> markers = <Marker>{};
    markers.add(Marker(
        markerId: MarkerId('geo-location'),
        position: LatLng(lat,lng),
      )
    );
    return Scaffold(
      appBar: AppBar(
        title: Text('MAPA'),
        actions: [
          IconButton(onPressed: () async{
            final GoogleMapController controller = await controlador.future;
            final position =CameraPosition(target: LatLng(lat,lng),zoom: 14.4746,tilt: 90.0,);
            controller.animateCamera(CameraUpdate.newCameraPosition(position));
          },
          icon: Icon(Icons.location_on))
        ],
      ),
      body: GoogleMap(
        markers: markers,
        mapType: MapType.normal,
        initialCameraPosition: puntoInicial,
        onMapCreated: (GoogleMapController controller) {
          controlador.complete(controller);
        },
      ),
      floatingActionButton:FloatingActionButton(
        onPressed: (){
          if(tipoMapa== MapType.normal){
            tipoMapa= MapType.satellite;
          }else{
            tipoMapa= MapType.normal;
          }
          setState(() {});
        },
        clipBehavior: Clip.hardEdge ,
        child: Icon(Icons.layers),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.miniStartFloat,
    );
  }
}