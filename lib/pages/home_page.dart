import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/pages/mapas_page.dart';
import 'package:qr_flutter/providers/scan_list_provider.dart';
import 'package:qr_flutter/providers/ui_provider.dart';
import 'package:qr_flutter/widgets/custom_navigatorbar.dart';
import 'package:qr_flutter/widgets/scan_botton.dart';

import 'direcciones_page.dart';

class HomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0, 
        title: Text('Historial'),
        actions: [
          IconButton(
            onPressed: (){
               Provider.of<ScanListProvider>(context,listen: false).borrarTodos();
            }, 
            icon: Icon(Icons.delete_forever)
          )
        ],
      ),
      body: _HomePageBody(),
      bottomNavigationBar: CustomNavigatorBar(),
      floatingActionButton: ScanBotton(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
} 

class _HomePageBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {  
    final uiProvider = Provider.of<UiProvider>(context);

    final scanListProvider = Provider.of<ScanListProvider>(context,listen: false);

    switch(uiProvider.selectedMenuOpt){ 
      case 0: 
        scanListProvider.cargarScansPorTipo('geo');
        return MapasPage();
      case 1: 
        scanListProvider.cargarScansPorTipo('http');
        return DireccionesPage(); 
      default: 
        scanListProvider.cargarScansPorTipo('geo');
        return MapasPage();
    }
  }
}