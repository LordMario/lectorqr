import 'package:flutter/material.dart';
import 'package:qr_flutter/widgets/scans_tiles.dart';

class DireccionesPage extends StatelessWidget {

  @override
   Widget build(BuildContext context) {
    return ScansTiles(tipo: 'http');
  }
}