import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/providers/scan_list_provider.dart';
import '../utils/utils.dart';

class ScanBotton extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () async{
        String barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          '#3DBBEF', 
          'Cancelar', 
          false, 
          ScanMode.QR
        );
        if(barcodeScanRes == '-1'){
          return;
        }
        final scaner = Provider.of<ScanListProvider>(context,listen: false);
        final result = await scaner.nuevoScan(barcodeScanRes);
        //
        launchURL(context,result);
      },
      child: Icon(Icons.search),
      elevation: 0,
    );
  }
} 