import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/providers/ui_provider.dart';

class CustomNavigatorBar extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    
    final uiProvider = Provider.of<UiProvider>(context);
    final index = uiProvider.selectedMenuOpt;
    return BottomNavigationBar(
      currentIndex: index,
      elevation: 0,
      items: [
        BottomNavigationBarItem(icon: Icon(Icons.map),label: 'Mapa'),
        BottomNavigationBarItem(icon: Icon(Icons.web), label: 'Direcciones')
      ],
      onTap:(int n){
        uiProvider.selectedMenuOption= n;
      } ,
    );
  }
}