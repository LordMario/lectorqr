import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/utils/utils.dart';
import '../providers/scan_list_provider.dart';

class ScansTiles extends StatelessWidget {
  final String tipo;

  const ScansTiles({required this.tipo});

  @override
  Widget build(BuildContext context) {
    final proveedor = Provider.of<ScanListProvider>(context);
    final lista= proveedor.scans;
    return ListView.builder(
      itemCount: lista.length,
      itemBuilder: ( _ , i)=> Dismissible(
        key: UniqueKey(),
        direction: DismissDirection.startToEnd,
        background: Icon(Icons.delete),
        onDismissed: (DismissDirection dis){
          proveedor.borraScansPorId(lista[i].id!);
        },
        child: ListTile(
          leading: tipo=='geo' ? Icon(Icons.map,color: Theme.of(context).primaryColor) :Icon(Icons.http,color: Theme.of(context).primaryColor),
          title: Text(lista[i].valor),
          subtitle: Text(lista[i].id.toString()),
          trailing: Icon(Icons.arrow_forward_ios_outlined,color: Colors.grey),
          onTap: (){
            launchURL(context,lista[i]);
          },
        ),
      ),
    );
  }
  
}