import 'package:flutter/material.dart';
import 'package:qr_flutter/providers/db_provider.dart';

class ScanListProvider extends ChangeNotifier{
  List <ScanModel> scans= [];

  String tipoSeleccionado = 'http';

  Future<ScanModel> nuevoScan(String value) async{
    final nuevoScan = ScanModel(valor: value);
    final id = await DBProvider.db.nuevoScan(nuevoScan);
    nuevoScan.id=id;

    if(tipoSeleccionado == nuevoScan.tipo){
      scans.add(nuevoScan);
      notifyListeners();
    }
    return nuevoScan;
  }

  cargarScans() async{
    final lista = await DBProvider.db.getAllScans();
    scans = [...lista];
    notifyListeners();
  }

  cargarScansPorTipo(String tipo) async{
    final lista = await DBProvider.db.getScansByTipo(tipo);
    scans =[...lista];
    tipoSeleccionado = tipo;
    notifyListeners();
    
  }
  borrarTodos()async{
    await DBProvider.db.deleteAllScans();
    scans= [];
    notifyListeners();
  }
  borraScansPorId(int id)async{
    await DBProvider.db.deleteScan(id);
    scans.removeWhere((x)=>x.id==id);
  }

   

}