import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:qr_flutter/models/scan_model.dart';
export 'package:qr_flutter/models/scan_model.dart';
import 'package:sqflite/sqflite.dart';
// ignore: depend_on_referenced_packages
import 'package:path/path.dart';

class DBProvider{
  static Database? _database;

  static final DBProvider db = DBProvider._();

  DBProvider._();

  Future<Database?> get database async {
    if(_database != null) return _database;

    _database = await initDB();
    return _database;
  }

  Future <Database?> initDB() async{
   // Directory documentsDirectory = await getApplicationDocumentsDirectory();
     
    Directory? documentsDirectory = await getExternalStorageDirectory();
    if(documentsDirectory!= null){
      final path = join( documentsDirectory.path, 'ScansDB.db');
      print(path);
      return await openDatabase(
        path,
        version: 1,
        onOpen: (db){},
        onCreate: (Database db, int version) async{
          await db.execute('''
            CREATE TABLE Scans(
              id INTEGER PRIMARY KEY,
              tipo TEXT,
              valor TEXT
            )
          ''');
        }
      );
    }else{
      return null;
    }
    
  }

  Future <int> nuevoScanRaw(ScanModel nuevoScan) async{
    final db =  await database;

    if(db!=null){
      final resp=await db.rawInsert('''
        Insert into Scans(id,tipo,valor)
        values( ${nuevoScan.id}, '${nuevoScan.tipo}', '${nuevoScan.valor}')
      ''');
      return resp;
    }
    return 0;
  }

  Future <int> nuevoScan(ScanModel nuevoScan) async{
    final db= await database;
    if(db!= null){
      final resp= await db.insert('Scans', nuevoScan.toJson());
      return resp;
    }
    return 0;
  }

  Future <ScanModel?> getRawScanById(int i) async {
    final db =await database;
    if(db != null){
      final resp = await db.rawQuery('''
        Select * from Scans 
        where id = $i
      ''');

      return resp.isNotEmpty ? ScanModel.fromJson(resp.first) : null;
    }
    return null;
  }

  Future <ScanModel?> getScanById(int id) async{
    final db= await database;
    if(db!= null){
      final resp = await db.query('Scans',where:'id = ?',whereArgs: [id]);
      return resp.isNotEmpty ? ScanModel.fromJson(resp.first) : null;
    }
    return null;
  }

  Future <List <ScanModel>> getAllScans() async{
    final db= await database;
    if(db!=null){
      final resp = await db.query('Scans');
      return resp.isNotEmpty ? resp.map((e) => ScanModel.fromJson(e)).toList() : [];
    }
    return [];
  }

  Future <List <ScanModel>> getScansByTipo (String tipo) async{
    final db = await database;
    if(db != null){
      final resp = await db.query('Scans',where: 'tipo= ?' , whereArgs: [tipo]);
      return resp.isNotEmpty ? resp.map((e) => ScanModel.fromJson(e)).toList() : [];
    }
    return [];
  } 

  Future <int> updateScan(ScanModel actualizarScan) async{
    final db= await database;
    if(db!= null){
      final resp= await db.update('Scans',actualizarScan.toJson(), where: 'id=?',whereArgs: [actualizarScan.id]);
      return resp;
    }
    return 0;
  }

  Future <int> deleteScan(int id) async{
    final db= await database;
    if(db != null){
      final resp = await db.delete('Scans',where: 'id=?',whereArgs: [id]);
      return resp;
    }
    return 0;
  }

  Future <int> deleteAllScans() async{
    final db= await database;
    if(db != null){
      final resp= await db.rawDelete('''
        Delete from Scans
      ''');
      return resp;
    }
    return 0;
  }
}