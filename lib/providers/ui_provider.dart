import 'package:flutter/material.dart';

class UiProvider extends ChangeNotifier{
  int selectedMenuOpt=0;

  int get selectedMenuOption{
    return selectedMenuOpt;
  }

  set selectedMenuOption(int indice){
    selectedMenuOpt= indice;
    notifyListeners();
  }
}