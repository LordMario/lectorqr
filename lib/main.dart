import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/pages/home_page.dart';
import 'package:qr_flutter/pages/mapa_page.dart';
import 'package:qr_flutter/providers/scan_list_provider.dart';
import 'package:qr_flutter/providers/ui_provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget { 
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_)=> UiProvider()),
        ChangeNotifierProvider(create: (_)=> ScanListProvider()),
      ],
      child: MaterialApp(
        title: 'Qr Reader',
        debugShowCheckedModeBanner: false,
        initialRoute: 'home',
        routes:{
          'home' : (_)=> HomePage(),
          'mapa' : (_)=> MapaPage() 
        },  
        theme: ThemeData(
          colorSchemeSeed: Colors.purpleAccent,
          floatingActionButtonTheme: FloatingActionButtonThemeData(
            backgroundColor: Colors.purpleAccent
          )
        ),
      ),
    );
  }
}