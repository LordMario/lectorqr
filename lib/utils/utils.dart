import 'package:flutter/cupertino.dart';
import 'package:qr_flutter/models/scan_model.dart';
import 'package:url_launcher/url_launcher.dart';

launchURL(BuildContext context ,ScanModel scan) async {
  if(scan.tipo=='http'){
    await launchUrl(Uri.parse(scan.valor),mode: LaunchMode.platformDefault );
  }else{
    Navigator.pushNamed(context, 'mapa', arguments: scan);
  }
}